all: prog

%: %.cc
	g++ -std=c++11 $< -o $@

%: %.c
	gcc -std=c11 $< -lm -o $@

