#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main(){
    srand(time(NULL));
    int numAtt, numData, numGen, i, j, genCount;
    float temp;
    float **data, *dot, *real, *weights, *predicted, *predError, *predDelta, *weightDelta, **transpose;
    
    
    scanf("%i %i %i", &numAtt, &numData, &numGen);
    
    data = (float**)calloc(numData, sizeof(float*));
    transpose = (float**)calloc(numData, sizeof(float*));
    
    for(i = 0; i < numData; i++){
        
        data[i] = (float*)calloc(numAtt, sizeof(float));
        transpose[i] = (float*)calloc(numAtt, sizeof(float));
        
        for(j = 0; j < numAtt; j++){
        
            scanf("%f",&temp);
            data[i][j] = temp;
        
        }   
        
    }
    
    real = (float*)calloc(numData, sizeof(float));
    
    for (i = 0; i < numData; i++){
     
        scanf("%f",&temp);
        real[i] = temp;
        
    }
    
    weights = (float*)calloc(numAtt, sizeof(float));
    
    for (i = 0; i < numAtt; i++){
     
        weights[i] = (rand()%101)/(100.0);
        
    }
    
    real = (float*)calloc(numData, sizeof(float));
    
    for (i = 0; i < numData; i++){
     
        weights[i] = (rand()%101)/(100.0);
        
    }
    
    predError = (float*)calloc(numData, sizeof(float));
    predDelta = (float*)calloc(numData, sizeof(float));
    weightDelta = (float*)calloc(numData, sizeof(float));
    transpose = (float**)calloc(numData, sizeof(float*));
    
    for (genCount = 0; genCount < numGen; i++){
        
        for(i = 0; i < numData; i++){
        
            dot[i] = 0;
            
            for(j = 0; j < numAtt; j++){
            
                dot[i] += data[i][j] * real[i];
            
            }   
        
        }
        
        for (i = 0; i < numData; i++){
         
            predicted[i] = 1/(1+exp(0-dot[i]));
            
        }
        
        for (i = 0; i < numData; i++){
         
            predError[i] = real[i] - predicted[i];
            
        }
        
        for (i = 0; i < numData; i++){
         
            predDelta[i] = predError[i]* (predicted[i]*(1-predicted[i]));
            
        }
        
        
        for(i = 0; i < numData; i++){
            
            
            
            for(j = 0; j < numAtt; j++){
            
                transpose[i][j] = data[j][i];
            
            }   
        
        }
        
        
        for(i = 0; i < numData; i++){
        
            weightDelta[i] = 0;
            
            for(j = 0; j < numAtt; j++){
            
                weightDelta[i] += transpose[i][j] * predDelta[i];
            
            }   
        
        }
        
        
        for (i = 0; i < numData; i++){
         
            weights[i] += weightDelta[i];
            
        }
        
       
        
    }
    
    return 0;
    
    
    
}